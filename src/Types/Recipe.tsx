type Recipe = {
	id : number,
	title: string,
	description: string,
	ingredients: number[]
}

export default Recipe;