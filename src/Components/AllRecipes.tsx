import React, { Component } from 'react'; // let's also import Component
import { Link } from 'react-router-dom'

import Recipe from '../Types/Recipe';
import Ingredient from '../Types/Ingredient';
import RecipeForm from './RecipeForm';
import { getRecipes } from '../fakeAPI';


const RecipeView = ({id, title, description, ingredients}:Recipe) => 
	<div>
		<h3> {title} </h3>
		<Link to={"/recipes/"+String(id)}><button>Dettagli</button></Link>
	</div>;


type AllRecipesSate = {
	recipes : Recipe[]
}

export default class AllRecipes extends Component<any, AllRecipesSate> {
	state : AllRecipesSate = {
		recipes : getRecipes()
	}

	// render will know everything!
	render() {
		return (
			<div>
				<h1> RICETTARIO </h1>
				<ol>
					{ getRecipes().map((recipe)=><li key = {"r_"+String(recipe.id)}> <RecipeView {...recipe} /> </li>)}
				</ol>
				<Link to="add-recipe"><button> Nuova Ricetta</button></Link>
				<RecipeForm added_callback = {() => {this.setState({recipes : getRecipes()})}} />
			</div>
		);
	}
}

export {RecipeView};