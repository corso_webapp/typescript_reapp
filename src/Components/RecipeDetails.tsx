import React, { Component } from 'react'; // let's also import Component
import { Link, Redirect } from 'react-router-dom'

import Recipe from '../Types/Recipe';
import Ingredient from '../Types/Ingredient';
import {getIngredients, getRecipes, deleteRecipe} from '../fakeAPI';

type RecipeDetailsStatus = {
	recipe: Recipe | undefined,
	redirect : boolean
}

export default class RecipeDetails extends Component<any, RecipeDetailsStatus> {
	state : RecipeDetailsStatus = {
		recipe : undefined,
		redirect : false
	}

	componentDidMount() {
		// il + è un cast ad intero
		this.setRecipe(+this.props.match.params.id);
	}

	componentDidUpdate(prevProps : any) {
  		// Typical usage (don't forget to compare props):
  		if (this.props.match.params.id !== prevProps.match.params.id ) {
			// il + è un cast ad intero
    		this.setRecipe(+this.props.match.params.id);
  		}
	}

	setRecipe(id : number) {
		let recipe_id : number = +this.props.match.params.id;
		for (let recipe of getRecipes()){
			if (recipe_id === recipe.id){
				this.setState({recipe: recipe});
				return;
			}
		}
		// set an undefined state
		this.setState({recipe: undefined});
		return;
	}

	deleteCurrentRecipe(){
		deleteRecipe(this.state.recipe!.id); 
		this.setState({redirect : true});
	}

	// render the component
	render() {
		// redirect is enabled!
		if (this.state.redirect) {
			return (<Redirect to = "/recipes" />);
		}
		// check if there is a recipe, if there is show it
		if (this.state.recipe){
			return (
				<div>
					<h1> {this.state.recipe!.title} </h1>
					<h3> Preparazione </h3>
					<p> {this.state.recipe!.description} </p>
					<h3> Ingredienti </h3>
					<ul>
						{this.state.recipe!.ingredients.map( (ingredient_id)=>{
							let ingredient_name = "Una Secchiata d'Aria";
							for (let ingredient of getIngredients()){
								if (ingredient_id === ingredient.id){
									ingredient_name = ingredient.name;
									break;
								}
							}
							return (<li key={"i_"+String(ingredient_id)} ><p>{ingredient_name}</p></li>);
						})}
					</ul>
					<button onClick = {() => this.deleteCurrentRecipe()} > Delete Recipe </button>
				</div>
			);
		}
		// there is no recipe with given id
		return (
			<div>
				<h1> I should redirect you to 404 page, but I don't have it.. :D </h1>
			</div>
		);
	}
}
