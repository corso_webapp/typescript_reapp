import React, { Component } from 'react'; // let's also import Component

import Recipe from '../Types/Recipe';
import Ingredient from '../Types/Ingredient';
import {getIngredients, getRecipes} from '../fakeAPI';
import {RecipeView} from './AllRecipes';

type RecipeRandomizerState = {
	time: Date,
	recipe: Recipe | undefined,
	nothingFound: boolean

}


export default class RecipeRandomizer extends Component<any, RecipeRandomizerState> {
	state : RecipeRandomizerState ={
		time : new Date(),
		recipe : undefined,
		nothingFound : false
	}

	clock_interval : any;

	// The tick function sets the current state. TypeScript will let us know
	// which ones we are allowed to set.
	tick() {
		this.setState({
			time: new Date()
		});
	}

	// After the component did mount, we set the state each second.
	componentDidMount() {
		this.clock_interval = setInterval(() => this.tick(), 1000);
	}

	componentWillUnmount() {
		clearInterval(this.clock_interval);
	}

	randomize(){
		this.setState({nothingFound:false});

		let e1 : any = document.getElementById("ingredient1");
		let e2 : any = document.getElementById("ingredient2");

		let i1 : number = +e1.options[e1.selectedIndex].value;
		let i2 : number = +e2.options[e2.selectedIndex].value;

		let selected_ingredients : number[] = [i1, i2];

		let available : Recipe[] = getRecipes().filter(({id, title, description, ingredients}:Recipe)=>{
			for(let ingre of selected_ingredients){
				if (ingredients.includes(ingre)){
					return true;
				}
			}
			return false;
		});

		if (available.length > 0){
			let chosen = available[Math.floor(Math.random() * available.length)];
			this.setState({recipe:chosen});
		}
		else{
			this.setState({nothingFound:true});
		}
	}

	// render will know everything!
	render() {
		return (
			<div>
				<h1>Ultra Meka Recipe Randomizer 2019!</h1>
				<p>It's {this.state.time.toLocaleTimeString()}, decide the menu! </p>
				
				{this.state.recipe
					? <div> <h3>Scelto per te oggi:</h3> <RecipeView {...this.state.recipe}/> </div>
					: this.state.nothingFound
						? <div> <h3>Nessuna Ricetta trovata con gli ingredienti selezionati.</h3> </div>
						: <div> <h3>Dicci quello che hai in frigo, e noi ti diremo chi sei.</h3> </div>
				}
				<p>Ingrediente #1</p>
				<div>
					<select id="ingredient1">
						<option value = {-1}> </option>
						{getIngredients().map(({id, name}: Ingredient) => <option key={"opt_1_"+String(id)} value={id}>{name}</option>)}
					</select>
					<p>Ingrediente #2</p>
					<select id="ingredient2">
						<option value = {-1}> </option>
						{getIngredients().map(({id, name}: Ingredient) => <option key={"opt_2_"+String(id)} value={id}>{name}</option>)}
					</select>  
				</div>
				<button onClick={() => this.randomize()}> Randomizza!! </button>
			</div>
		);
	}
}
