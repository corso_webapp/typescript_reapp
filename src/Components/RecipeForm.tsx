import React, { Component } from 'react'; // let's also import Component
import { Link } from 'react-router-dom'

import Recipe from '../Types/Recipe';
import Ingredient from '../Types/Ingredient';
import {getIngredients, getRecipes, addRecipe} from '../fakeAPI';


type RecipeFormStatus = {
	operation_successful : boolean,
}


export default class RecipeForm extends Component<any, RecipeFormStatus> {
	state : RecipeFormStatus ={
		operation_successful:false
	}

	addNewRecipe(){
		this.setState({operation_successful:false});
		let t : string =  (document.getElementById("title_input") as HTMLInputElement).value;
		let d : string =  (document.getElementById("descr_input") as HTMLInputElement).value;
		let i1: number = +(document.getElementById("ingredient1") as HTMLInputElement).value;
		let i2: number = +(document.getElementById("ingredient2") as HTMLInputElement).value;

		let i : number[] = [];
		if (i1 >= 0){ i.push(i1)};
		if (i2 >= 0){ i.push(i2)};
		if(t && d && i.length > 0){
			addRecipe(t, d, i);
			this.setState({operation_successful:true});
			if (this.props.added_callback){
				this.props.added_callback();
			}
		}

	}

	render() {
		return (
			<div>
				{this.state.operation_successful ? <p>Ricetta Aggiunta con Successo!</p> : null}
				<h3>Inserisci una nuova ricetta!</h3>

				<p><b>Titolo</b></p>
				<input id="title_input"></input>
				<p><b>Descrizione</b></p>
				<textarea id="descr_input" rows={3} cols={20}></textarea>
				<p><b>ingrediente#1</b></p>
				<select id="ingredient1">
					<option value = {-1}> </option>
					{getIngredients().map(({id, name}: Ingredient) => <option key={"opt_1_"+String(id)} value={id}>{name}</option>)}
				</select>
				<p><b>ingrediente#2</b></p>
				<select id="ingredient2">
					<option value = {-1}> </option>
					{getIngredients().map(({id, name}: Ingredient) => <option key={"opt_2_"+String(id)} value={id}>{name}</option>)}
				</select> 
				<div>
					<button onClick={()=>this.addNewRecipe()}>Conferma</button>
					<Link to = "/recipes"><button>Indietro</button></Link>
				</div>

			</div>
		);
	}
}
