import Ingredient from './Types/Ingredient'
import Recipe from './Types/Recipe'

let allIngredients : Ingredient[] = [
	{
		id : 0,
		name : "Fagioli"
	},
	{
		id : 1,
		name : "Pasta"
	},
	{
		id : 2,
		name : "Piselli"
	},
	{
		id : 3,
		name : "Riso"
	},
	{
		id : 4,
		name : "Pomodoro"
	},
	{
		id : 5,
		name : "Prosciutto"
	},
	{
		id : 6,
		name : "Zucchine"
	}
]

let allRecipes : Recipe[] = [
	{
		id: 0,
		title: "Pasta e Fagioli",
		description: "Cuoci la Pasta e i Fagioli.",
		ingredients: [0, 1]
	},
	{
		id: 1,
		title: "Risi e Bisi",
		description: "Cuoci il riso con i Piselli.",
		ingredients: [2, 3]
	},
	{
		id: 2,
		title: "Pasta al pomodoro",
		description: "Cuoci la pasta e il pomodoro a separatamente. Se sei inglese invece, cuoci tutto assieme.",
		ingredients: [1, 4]
	},
	{
		id: 3,
		title: "Risotto con le zucchine",
		description: "Taglia le zucchine a pezzetti. Butta in pentola assieme al riso e bla. Poi bla bla bla..",
		ingredients: [3, 6]
	},
	{
		id: 4,
		title: "Involtini di prosciutto e zucchine",
		description: "Prendi il prosciutto poi .. uvshwik ..casheiivk ... ptriff!. .. destra e te sì rivà.",
		ingredients: [5, 6]
	}
]

function getRecipes() : Recipe[] {
	return allRecipes;
}

function getIngredients() : Ingredient[] {
	return allIngredients;
}

function addRecipe(title: string, description: string, ingredients: number[]) : Recipe {
	let new_id : number = allRecipes[allRecipes.length-1].id + 1;
	let new_recipe : Recipe = {id: new_id, title: title, description: description, ingredients: ingredients};
	allRecipes.push(new_recipe);
	return (new_recipe);
}

function deleteRecipe(id: number) : any {
    for( let recipe of allRecipes){
    	if(recipe.id === id){
    		allRecipes.splice(allRecipes.indexOf(recipe), 1);
    	}
    }
}

function addIngredient(title: string, description: string, ingredients: number[]) : Recipe {
	let new_id : number = allRecipes[-1].id + 1;
	let new_recipe : Recipe = {id: new_id, title: title, description: description, ingredients: ingredients};
	allRecipes.push(new_recipe);
	return (new_recipe);
}

function deleteIngredient(id: number) : any {
    for( let ingredient of allIngredients){
    	if(ingredient.id === id){
    		allIngredients.splice(allIngredients.indexOf(ingredient), 1);
    	}
    }
}

export {getIngredients, getRecipes, addRecipe, deleteRecipe, addIngredient, deleteIngredient}