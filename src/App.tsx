import React from 'react';
import './App.css';
import {BrowserRouter as Router, Link , Route} from 'react-router-dom' // install router with:  npm install @types/react-router-dom

import RecipeRandomizer from './Components/RecipeRandomizer'
import AllRecipes from './Components/AllRecipes'
import RecipeDetails from './Components/RecipeDetails'
import RecipeForm from './Components/RecipeForm';


const App: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header">
      	<Router>
      		<Route exact path = '/' component = {(props : any) => <RecipeRandomizer {...props}/>}/>
          <Route exact path = '/recipes' component = {(props: any) => <AllRecipes {...props}/>}/>
      		<Route exact path = '/recipes/:id' component = {(props: any) => <RecipeDetails {...props}/>}/>
          <Route exact path = '/add-recipe' component = {(props: any) => <RecipeForm {...props}/>}/>
        </Router>
      </header>
    </div>
  );
}

export default App;
